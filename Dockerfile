FROM alpine
LABEL maintainer="Aung Myat Kyaw <aungmyatkyaw.kk@gmail.com>"
RUN apk --update add --no-cache openjdk17-jdk maven curl \
    && wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub \
    && wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.35-r1/glibc-2.35-r1.apk \
    && apk add glibc-2.35-r1.apk \
    && rm -f glibc-2.35-r1.apk
ENV JAVA_HOME="/usr/lib/jvm/java-17-openjdk" M2_HOME="/usr/share/java/maven"
ENV PATH=${JAVA_HOME}/bin:${M2_HOME}/bin:${PATH}
